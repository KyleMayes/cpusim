// Copyright 2016 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CPUSIM_FORMAT_HPP
#define CPUSIM_FORMAT_HPP

#include <iostream>
#include <string>

#include <boost/format.hpp>

namespace cpusim
{

namespace detail
{
    inline void format(boost::format&) { }

    template <typename T, typename... N>
    void format(boost::format& formatter, T&& argument, N&&... arguments)
    {
        formatter % argument;
        format(formatter, std::forward<N>(arguments)...);
    }
}

template <typename... N>
std::string format(const char* specification, N&&... arguments)
{
    boost::format formatter{specification};
    detail::format(formatter, std::forward<N>(arguments)...);
    return formatter.str();
}

template <typename... N>
void log(const char* specification, N&&... arguments)
{
    std::cout << format(specification, std::forward<N>(arguments)...) << std::endl;
}

}

#endif
