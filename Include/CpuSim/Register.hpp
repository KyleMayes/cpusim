// Copyright 2016 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CPUSIM_REGISTER_HPP
#define CPUSIM_REGISTER_HPP

#include <CpuSim/StringView.hpp>

#include <cstdint>
#include <vector>

namespace cpusim
{

class Cpu;

/// A CPU register.
class Register
{
    std::string name;
    uint32_t value;

public:
    Register(std::string name);

    /// Returns the value of this register with the supplied CPU.
    uint32_t load(Cpu* cpu) const;

    /// Stores the supplied value in this register with the supplied CPU.
    void store(Cpu* cpu, uint32_t value);

private:
    friend std::ostream& operator<<(std::ostream& stream, const Register& register_);
};

std::ostream& operator<<(std::ostream& stream, const Register& register_);

/// A reference to a CPU register.
using RegisterRef = uint32_t;

/// A set of CPU registers.
class RegisterSet
{
    std::vector<Register> registers;

public:
    /// Returns a reference to the register with the supplied name.
    static RegisterRef get_ref(StringView name);

    RegisterSet();

    /// Returns the number of registers in this set.
    size_t size() const;

    /// Returns the value of the supplied register with the supplied CPU.
    uint32_t load(Cpu* cpu, RegisterRef ref) const;

    /// Stores the supplied value in the supplied register with the supplied CPU.
    void store(Cpu* cpu, RegisterRef ref, uint32_t value);

private:
    friend std::ostream& operator<<(std::ostream& stream, const RegisterSet& registers);
};

std::ostream& operator<<(std::ostream& stream, const RegisterSet& registers);

}

#endif
