// Copyright 2016 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CPUSIM_MEMORY_HPP
#define CPUSIM_MEMORY_HPP

#include <cstddef>
#include <cstdint>
#include <ostream>
#include <vector>

namespace cpusim
{

class Cpu;

class Memory
{
    std::vector<uint32_t> memory;

public:
    Memory(size_t size);

    /// Loads the value in the supplied memory address using the supplied CPU.
    uint32_t load(Cpu* cpu, uint32_t address) const;

    /// Loads the values in the supplied memory address using the supplied CPU.
    std::vector<uint32_t> load(Cpu* cpu, uint32_t address, size_t size) const;

    /// Stores the supplied value in the supplied memory address using the supplied CPU.
    void store(Cpu* cpu, uint32_t address, uint32_t value);

    /// Stores the supplied values in the supplied memory address using the supplied CPU.
    void store(Cpu* cpu, uint32_t address, const std::vector<uint32_t>& value);

private:
    friend std::ostream& operator<<(std::ostream& stream, const Memory& memory);
};

std::ostream& operator<<(std::ostream& stream, const Memory& memory);

}

#endif
