// Copyright 2016 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CPUSIM_PROGRAM_HPP
#define CPUSIM_PROGRAM_HPP

#include <CpuSim/Register.hpp>
#include <CpuSim/Instruction.hpp>

#include <array>
#include <unordered_map>

#include <boost/optional.hpp>
#include <boost/variant.hpp>

namespace cpusim
{

/// A mapping of labels to memory offsets.
using Offsets = std::unordered_map<StringView, uint32_t>;

/// A piece of the data segment of an assembly program.
struct Data
{
    std::vector<uint32_t> data;

    Data(uint32_t data);
    Data(std::vector<uint32_t> data);
};

using Argument = boost::variant<uint32_t, StringView>;

/// A piece of the text segment of an assembly program.
struct Text
{
    InstructionRef instruction;
    boost::optional<Argument> a, b, c;

    Text(StringView instruction);
    Text(StringView instruction, Argument a);
    Text(StringView instruction, Argument a, Argument b);
    Text(StringView instruction, Argument a, Argument b, Argument c);
};

/// An assembly program source.
struct Program
{
    std::vector<std::pair<boost::optional<StringView>, Data>> data;
    std::vector<std::pair<boost::optional<StringView>, Text>> text;

    Program() = default;

    /// Adds the supplied data to this assembly program source.
    void add(Data data);

    /// Adds the supplied text to this assembly program source.
    void add(Text text);

    /// Adds the supplied data with the supplied label to this assembly program source.
    void add(StringView label, Data data);

    /// Adds the supplied text with the supplied label to this assembly program source.
    void add(StringView label, Text text);

    /// Loads this assembly program into the supplied memory and returns the entry point.
    uint32_t load(std::vector<uint32_t>& memory, uint32_t offset, Offsets& offsets) const;
};

#define REGISTER(INSTR) ::cpusim::RegisterSet::get_ref(INSTR)

}

#endif
