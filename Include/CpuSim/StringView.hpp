// Copyright 2016 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CPUSIM_STRING_VIEW_HPP
#define CPUSIM_STRING_VIEW_HPP

#include <cassert>
#include <ostream>
#include <string>

#include <boost/functional/hash.hpp>

namespace cpusim
{

/// A view into a string allocated elsewhere.
template <typename C, typename T = std::char_traits<C>>
class BasicStringView
{
    const C* data_;
    size_t size_;

public:
    BasicStringView() : data_{nullptr}, size_{0} { }
    BasicStringView(const C* data) : data_{data}, size_{T::length(data)} { }
    BasicStringView(const C* data, size_t size) : data_{data}, size_{size} { }
    BasicStringView(const std::basic_string<C, T>& string) : data_{string.data()}, size_{string.size()} { }

    /// Returns the pointer to the start of this string view.
    const C* data() const
    {
        return data_;
    }

    /// Returns the size of this string view.
    size_t size() const
    {
        return size_;
    }

    C operator[](size_t index) const
    {
        assert(index < size_);
        return data_[index];
    }

    /// Returns this string view as a string.
    std::basic_string<C, T> str() const
    {
        return {data_, size_};
    }

    /// Returns an iterator at the beginning of this string view.
    const C* begin() const
    {
        return data_;
    }

    /// Returns an iterator after the end of this string view.
    const C* end() const
    {
        return data_ + size_;
    }
};

/// A string view for regular strings.
using StringView = BasicStringView<char>;

template <typename C, typename T>
bool operator==(BasicStringView<C, T> left, BasicStringView<C, T> right)
{
    return left.size() == right.size() && T::compare(left.data(), right.data(), left.size()) == 0;
}

template <typename C, typename T>
bool operator!=(BasicStringView<C, T> left, BasicStringView<C, T> right)
{
    return !operator==(left, right);
}

template <typename C, typename T>
std::ostream& operator<<(std::ostream& stream, BasicStringView<C, T> view)
{
    stream << view.str();
    return stream;
}

}

namespace std
{

template <typename C, typename T>
struct hash<cpusim::BasicStringView<C, T>>
{
    size_t operator()(const cpusim::BasicStringView<C, T>& view) const
    {
        return boost::hash_range(view.begin(), view.end());
    }
};

}

#endif
