/// End program execution.
CPUSIM_INSTRUCTION(HALT, 0,
    [](Cpu*, InstructionArguments) {
    })

/// Do nothing.
CPUSIM_INSTRUCTION(NOOP, 0,
    [](Cpu*, InstructionArguments) {
    })

/// Sets the instruction pointer.
CPUSIM_INSTRUCTION(GOTO, 1,
    [](Cpu* cpu, InstructionArguments arguments) {
        cpu->store(RegisterSet::get_ref("IP"), arguments.a);
    })

/// Move a value into a register from an array.
CPUSIM_INSTRUCTION(IDXI, 1,
    [](Cpu* cpu, InstructionArguments arguments) {
        cpu->store(arguments.a, cpu->load(arguments.b + cpu->load(arguments.c)));
    })

/// Move a value into an array from a register.
CPUSIM_INSTRUCTION(IDXO, 1,
    [](Cpu* cpu, InstructionArguments arguments) {
        cpu->store(arguments.a + cpu->load(arguments.b), cpu->load(arguments.c));
    })

/// Move a value into an address/register from another address/register.
CPUSIM_INSTRUCTION(MOVE, 1,
    [](Cpu* cpu, InstructionArguments arguments) {
        cpu->store(arguments.a, cpu->load(arguments.b));
    })

/// Print the value in an address/register.
CPUSIM_INSTRUCTION(PRNT, 1,
    [](Cpu* cpu, InstructionArguments arguments) {
        auto value = cpu->load(arguments.a);
        std::cout << ">> " << value << std::endl;
    })

/// Increment the address/register.
CPUSIM_INSTRUCTION(INCR, 1,
    [](Cpu* cpu, InstructionArguments arguments) {
        cpu->store(arguments.a, cpu->load(arguments.a) + 1);
    })

/// Decrement the address/register.
CPUSIM_INSTRUCTION(DECR, 1,
    [](Cpu* cpu, InstructionArguments arguments) {
        cpu->store(arguments.a, cpu->load(arguments.a) - 1);
    })

#define CPUSIM_BRANCH_INSTRUCTION(NAME, OPERATOR) \
    CPUSIM_INSTRUCTION(NAME, 1, \
        [](Cpu* cpu, InstructionArguments arguments) { \
            if (cpu->load(arguments.a) OPERATOR cpu->load(arguments.b)) \
            { \
                cpu->store(RegisterSet::get_ref("IP"), arguments.c); \
            } \
        })

CPUSIM_BRANCH_INSTRUCTION(BREQ, ==)
CPUSIM_BRANCH_INSTRUCTION(BRNE, !=)
CPUSIM_BRANCH_INSTRUCTION(BRLT, <)
CPUSIM_BRANCH_INSTRUCTION(BRLE, <=)
CPUSIM_BRANCH_INSTRUCTION(BRGT, >)
CPUSIM_BRANCH_INSTRUCTION(BRGE, >=)

#undef CPUSIM_BRANCH_INSTRUCTION

#define CPUSIM_UNSIGNED_ARITHMETIC_INSTRUCTION(NAME, CYCLES, OPERATOR) \
    CPUSIM_INSTRUCTION(NAME, CYCLES, \
        [](Cpu* cpu, InstructionArguments arguments) { \
            cpu->store(arguments.a, cpu->load(arguments.b) OPERATOR cpu->load(arguments.c)); \
        })

CPUSIM_UNSIGNED_ARITHMETIC_INSTRUCTION(UADD, 4, +)
CPUSIM_UNSIGNED_ARITHMETIC_INSTRUCTION(USUB, 4, -)
CPUSIM_UNSIGNED_ARITHMETIC_INSTRUCTION(UMUL, 4, *)
CPUSIM_UNSIGNED_ARITHMETIC_INSTRUCTION(UDIV, 4, /)

#undef CPUSIM_UNSIGNED_ARITHMETIC_INSTRUCTION
