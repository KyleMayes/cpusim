// Copyright 2016 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CPUSIM_INSTRUCTION_HPP
#define CPUSIM_INSTRUCTION_HPP

#include <CpuSim/Register.hpp>

#include <cstdint>
#include <functional>
#include <vector>

namespace cpusim
{

/// The arguments to a CPU instruction.
struct InstructionArguments
{
    uint32_t a, b, c;
};

/// A function implementing a CPU instruction.
using InstructionFunction = std::function<void(Cpu*, InstructionArguments)>;

/// A CPU instruction.
class Instruction
{
    std::string name;
    size_t cycles;
    InstructionFunction function;

public:
    Instruction(std::string name, size_t cycles, InstructionFunction function);

    /// Executes this instruction with the supplied CPU.
    void execute(Cpu* cpu, InstructionArguments arguments) const;
};

/// A reference to a CPU register.
using InstructionRef = uint32_t;

/// A set of CPU instructions.
class InstructionSet
{
    std::vector<Instruction> instructions;

public:
    /// Returns a reference to the register with the supplied name.
    static InstructionRef get_ref(StringView name);

    InstructionSet();

    /// Executes the supplied instruction with the supplied CPU.
    void execute(Cpu* cpu, InstructionRef ref, InstructionArguments arguments) const;
};

/// A CPU instruction as it is stored in memory.
using RawInstruction = std::array<uint32_t, 4>;

}

#endif
