// Copyright 2016 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CPUSIM_CPU_HPP
#define CPUSIM_CPU_HPP

#include <CpuSim/Memory.hpp>
#include <CpuSim/Program.hpp>

namespace cpusim
{

/// A cache request where the bool indicates whether there was a cache miss.
template <typename T>
using Request = std::pair<T, bool>;

/// An interface for an instruction cache.
class InstructionCache
{
public:
    virtual ~InstructionCache() = default;

    /// Returns the instruction at the supplied memory address.
    virtual Request<RawInstruction> load(Cpu* cpu, Memory& memory, uint32_t address) = 0;
};

/// An interface for a data cache.
class DataCache
{
public:
    virtual ~DataCache() = default;

    /// Returns the word at the supplied memory address.
    virtual Request<uint32_t> load(Cpu* cpu, Memory& memory, uint32_t address) = 0;
};

/// A simulated central processing unit (CPU).
class Cpu
{
    InstructionSet instructions;
    RegisterSet registers;
    size_t cycles;
    Memory memory;
    std::unique_ptr<InstructionCache> icache;
    std::unique_ptr<DataCache> dcache;
    size_t icachereq, icachemiss, dcachereq, dcachemiss;

public:
    Cpu(Memory memory, std::unique_ptr<InstructionCache> icache, std::unique_ptr<DataCache> dcache);

    /// Logs the supplied message.
    void log(StringView message);

    /// Returns the number of cycles this CPU has executed for.
    size_t get_cycles() const;

    /// Consume the supplied number of cycles.
    void consume(size_t cycles);

    /// Returns the value of the supplied register or memory address.
    uint32_t load(uint32_t address);

    /// Stores the supplied value in the supplied register or memory address.
    void store(uint32_t address, uint32_t value);

    /// Returns the memory in used by this CPU.
    const Memory& dump() const;

    /// Loads the supplied assembly program into memory and executes it.
    void execute(const Program& program);

private:
    /// Runs a program loaded into memory which begins at the supplied entry point.
    void run(uint32_t entry);

    friend std::ostream& operator<<(std::ostream& stream, const Cpu& cpu);
};

std::ostream& operator<<(std::ostream& stream, const Cpu& cpu);

}

#endif
