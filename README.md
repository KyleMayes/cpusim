# CpuSim

A simple CPU simulator.

Released under the Apache License 2.0.

## Build Instructions

Requirements:

* CMake 2.8 or later
* A C++14 compatible compiler and standard library

```
$ mkdir Build
$ cd Build
$ cmake ..
$ make
```
