#include <CpuSim.hpp>
namespace cs = cpusim;

#include "Program/ArrayAdd.hpp"

/// An instruction cache that doesn't actually do any caching.
class PassthroughInstructionCache : public cs::InstructionCache
{
public:
    cs::Request<cs::RawInstruction> load(cs::Cpu* cpu, cs::Memory& memory, uint32_t address) override
    {
        auto i = memory.load(cpu, address, 4);
        return {{{i[0], i[1], i[2], i[3]}}, true};
    }
};

/// A data cache that doesn't actually do any caching.
class PassthroughDataCache : public cs::DataCache
{
public:
    cs::Request<uint32_t> load(cs::Cpu* cpu, cs::Memory& memory, uint32_t address) override
    {
        return {memory.load(cpu, address), true};
    }
};

int main()
{
    auto icache = std::make_unique<PassthroughInstructionCache>();
    auto dcache = std::make_unique<PassthroughDataCache>();
    cs::Cpu cpu{cs::Memory{256}, std::move(icache), std::move(dcache)};

    cpu.execute(array_add());
    std::cout << cpu;

    return 0;
}
