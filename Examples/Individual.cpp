#include <CpuSim.hpp>
namespace cs = cpusim;

#include "Program/ArrayAdd.hpp"

/// An instruction cache that caches the most recently used instructions.
template <size_t N>
class IndividualInstructionCache : public cs::InstructionCache
{
    std::deque<std::pair<uint32_t, cs::RawInstruction>> cache;

public:
    cs::Request<cs::RawInstruction> load(cs::Cpu* cpu, cs::Memory& memory, uint32_t address) override
    {
        for (const auto& instruction : cache)
        {
            if (instruction.first == address)
            {
                return {instruction.second, false};
            }
        }
        if (cache.size() == N)
        {
            cache.pop_front();
        }
        auto i = memory.load(cpu, address, 4);
        cs::RawInstruction raw {{i[0], i[1], i[2], i[3]}};
        cache.emplace_back(address, raw);
        return {raw, true};
    }
};

/// A data cache that caches the most recently used words.
template <size_t N>
class IndividualDataCache : public cs::DataCache
{
    std::deque<std::pair<uint32_t, uint32_t>> cache;

public:
    cs::Request<uint32_t> load(cs::Cpu* cpu, cs::Memory& memory, uint32_t address) override
    {
        for (const auto& word : cache)
        {
            if (word.first == address)
            {
                return {word.second, false};
            }
        }
        if (cache.size() == N)
        {
            cache.pop_front();
        }
        auto word = memory.load(cpu, address);
        cache.emplace_back(address, word);
        return {word, true};
    }
};

int main()
{
    auto icache = std::make_unique<IndividualInstructionCache<8>>();
    auto dcache = std::make_unique<IndividualDataCache<32>>();
    cs::Cpu cpu{cs::Memory{256}, std::move(icache), std::move(dcache)};

    cpu.execute(array_add());
    std::cout << cpu;

    return 0;
}
