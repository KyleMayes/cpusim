/// An assembly program that adds two arrays and stores the result in a third array.
cs::Program array_add()
{
    cs::Program program;

    program.add("a",    cs::Data{std::vector<uint32_t>(17, 8)});
    program.add("b",    cs::Data{std::vector<uint32_t>(42, 8)});
    program.add("c",    cs::Data{std::vector<uint32_t>(00, 8)});
    program.add("size", cs::Data{8});

    program.add("main", cs::Text{"MOVE", REGISTER("R4"), "size"});
    program.add(        cs::Text{"GOTO", "loop"});
    program.add("loop", cs::Text{"IDXI", REGISTER("R0"), "a", REGISTER("R3")});
    program.add(        cs::Text{"IDXI", REGISTER("R1"), "b", REGISTER("R3")});
    program.add(        cs::Text{"UADD", REGISTER("R2"), REGISTER("R0"), REGISTER("R1")});
    program.add(        cs::Text{"IDXO", "c", REGISTER("R3"), REGISTER("R2")});
    program.add(        cs::Text{"INCR", REGISTER("R3")});
    program.add(        cs::Text{"BRLT", REGISTER("R3"), REGISTER("R4"), "loop"});
    program.add("halt", cs::Text{"HALT"});

    return program;
}
