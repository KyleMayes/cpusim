#include <CpuSim.hpp>
namespace cs = cpusim;

#include "Program/ArrayAdd.hpp"

/// An instruction cache that caches the most recently used instructions.
template <size_t N>
class IndividualInstructionCache : public cs::InstructionCache
{
    std::deque<std::pair<uint32_t, cs::RawInstruction>> cache;

public:
    cs::Request<cs::RawInstruction> load(cs::Cpu* cpu, cs::Memory& memory, uint32_t address) override
    {
        for (const auto& instruction : cache)
        {
            if (instruction.first == address)
            {
                return {instruction.second, false};
            }
        }
        if (cache.size() == N)
        {
            cache.pop_front();
        }
        auto i = memory.load(cpu, address, 4);
        cs::RawInstruction raw {{i[0], i[1], i[2], i[3]}};
        cache.emplace_back(address, raw);
        return {raw, true};
    }
};

/// A data cache that caches the most recently used cache lines.
template <size_t N, size_t M>
class LineDataCache : public cs::DataCache
{
    std::deque<std::pair<uint32_t, std::vector<uint32_t>>> cache;

public:
    cs::Request<uint32_t> load(cs::Cpu* cpu, cs::Memory& memory, uint32_t address) override
    {
        for (const auto& word : cache)
        {
            if (address >= word.first && word.first < address + M)
            {
                return {word.second[address - word.first], false};
            }
        }
        if (cache.size() == N)
        {
            cache.pop_front();
        }
        auto line = memory.load(cpu, address, M);
        cache.emplace_back(address, line);
        return {line[0], true};
    }
};

int main()
{
    auto icache = std::make_unique<IndividualInstructionCache<8>>();
    auto dcache = std::make_unique<LineDataCache<4, 8>>();
    cs::Cpu cpu{cs::Memory{256}, std::move(icache), std::move(dcache)};

    cpu.execute(array_add());
    std::cout << cpu;

    return 0;
}
