#include <CpuSim/Memory.hpp>

#include <CpuSim/Cpu.hpp>
#include <CpuSim/Format.hpp>

namespace cpusim
{

Memory::Memory(size_t size) : memory(size, 0) { }

uint32_t Memory::load(Cpu* cpu, uint32_t address) const
{
    assert(address < memory.size());
    cpu->log(format("Memory::load(): 0x%X", address));
    cpu->consume(32);
    return memory[address];
}

std::vector<uint32_t> Memory::load(Cpu* cpu, uint32_t address, size_t size) const
{
    assert(address + size < memory.size());
    cpu->log(format("Memory::load(): 0x%X", address));
    cpu->consume(32);
    return {memory.data() + address, memory.data() + address + size};
}

void Memory::store(Cpu* cpu, uint32_t address, uint32_t value)
{
    assert(address < memory.size());
    cpu->log(format("Memory::store(): 0x%X", address));
    cpu->consume(32);
    memory[address] = value;
}

void Memory::store(Cpu* cpu, uint32_t address, const std::vector<uint32_t>& value)
{
    assert(address + value.size() < memory.size());
    cpu->log(format("Memory::store(): 0x%X", address));
    cpu->consume(32);
    std::copy(value.begin(), value.end(), memory.begin() + address);
}

std::ostream& operator<<(std::ostream& stream, const Memory& memory)
{
    for (size_t i = 0; i < memory.memory.size(); ++i)
    {
        if (i != 0 && i % 8 == 0)
        {
            stream << std::endl;
        }
        if (i == 0 || i % 8 == 0)
        {
            stream << (boost::format{"%04d |> "} % i).str();
        }
        stream << (boost::format{"%08X "} % memory.memory[i]).str();
    }
    return stream;
}

}
