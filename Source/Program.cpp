#include <CpuSim/Program.hpp>

#include <CpuSim/Cpu.hpp>
#include <CpuSim/Format.hpp>

namespace cpusim
{

Data::Data(uint32_t data) : data{data} { }

Data::Data(std::vector<uint32_t> data) : data{std::move(data)} { }

Text::Text(StringView instruction) : instruction{InstructionSet::get_ref(instruction)} { }

Text::Text(StringView instruction, Argument a) : instruction{InstructionSet::get_ref(instruction)}, a{a} { }

Text::Text(StringView instruction, Argument a, Argument b) : instruction{InstructionSet::get_ref(instruction)}, a{a}, b{b} { }

Text::Text(StringView instruction, Argument a, Argument b, Argument c) : instruction{InstructionSet::get_ref(instruction)}, a{a}, b{b}, c{c} { }

void Program::add(Data data)
{
    this->data.emplace_back(boost::none_t{}, std::move(data));
}

void Program::add(Text text)
{
    this->text.emplace_back(boost::none_t{}, std::move(text));
}

void Program::add(StringView label, Data data)
{
    this->data.emplace_back(label, std::move(data));
}

void Program::add(StringView label, Text text)
{
    this->text.emplace_back(label, std::move(text));
}

uint32_t encode(const boost::optional<Argument>& argument, Offsets& offsets)
{
    if (!argument)
    {
        return 0;
    }

    if (argument->type() == typeid(uint32_t))
    {
        return boost::get<uint32_t>(*argument);
    }

    auto iterator = offsets.find(boost::get<StringView>(*argument));
    if (iterator == offsets.end())
    {
        log("ERROR: no such label: '%1%'", boost::get<StringView>(*argument));
        std::exit(1);
    }
    return iterator->second;
}

uint32_t Program::load(std::vector<uint32_t>& memory, uint32_t offset, Offsets& offsets) const
{
    // Load the program data into memory.
    for (const auto& data : data)
    {
        if (data.first)
        {
            offsets.emplace(*data.first, memory.size() + offset);
        }
        memory.insert(memory.end(), data.second.data.begin(), data.second.data.end());
    }

    // Load the program instructions into memory.
    auto start = memory.size();
    for (const auto& text : text)
    {
        if (text.first)
        {
            offsets.emplace(*text.first, start + offset);
        }
        start += 4;
    }
    for (const auto& text : text)
    {
        RawInstruction raw;
        raw[0] = text.second.instruction;
        raw[1] = encode(text.second.a, offsets);
        raw[2] = encode(text.second.b, offsets);
        raw[3] = encode(text.second.c, offsets);
        memory.insert(memory.end(), raw.begin(), raw.end());
    }

    // Find the entry point and begin executing the program.
    auto iterator = offsets.find("main");
    if (iterator == offsets.end())
    {
        log("ERROR: no entry point in assembly program");
        std::exit(1);
    }
    return iterator->second;
}

}
