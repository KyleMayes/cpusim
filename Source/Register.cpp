#include <CpuSim/Register.hpp>

#include <CpuSim/Cpu.hpp>
#include <CpuSim/Format.hpp>

namespace cpusim
{

Register::Register(std::string name) : name{std::move(name)}, value{0} { }

uint32_t Register::load(Cpu* cpu) const
{
    cpu->log(format("Register::load(): %s (%d)", name, value));
    return value;
}

void Register::store(Cpu* cpu, uint32_t value)
{
    cpu->log(format("Register::store(): %s (%d -> %d)", name, this->value, value));
    this->value = value;
}

std::ostream& operator<<(std::ostream& stream, const Register& register_)
{
    stream << register_.name << ": " << register_.value;
    return stream;
}

RegisterRef RegisterSet::get_ref(StringView name)
{
    RegisterRef counter = 0;
#define CPUSIM_REGISTER(NAME) if (name == StringView{#NAME}) { return counter; } counter += 1;
    #include <CpuSim/Register.inl>
#undef CPUSIM_REGISTER
    log("ERROR: no such register: '%1%'", name);
    std::exit(1);
}

RegisterSet::RegisterSet()
{
#define CPUSIM_REGISTER(NAME) registers.emplace_back(#NAME);
    #include <CpuSim/Register.inl>
#undef CPUSIM_REGISTER
}

size_t RegisterSet::size() const
{
    return registers.size();
}

uint32_t RegisterSet::load(Cpu* cpu, RegisterRef ref) const
{
    assert(ref < registers.size());
    return registers[ref].load(cpu);
}

void RegisterSet::store(Cpu* cpu, RegisterRef ref, uint32_t value)
{
    assert(ref < registers.size());
    registers[ref].store(cpu, value);
}

std::ostream& operator<<(std::ostream& stream, const RegisterSet& registers)
{
    for (const auto& register_ : registers.registers)
    {
        stream << register_ << std::endl;
    }
    return stream;
}

}
