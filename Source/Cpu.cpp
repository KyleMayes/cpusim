#include <CpuSim/Cpu.hpp>

#include <CpuSim/Format.hpp>

#include <limits>

namespace cpusim
{

Cpu::Cpu(Memory memory, std::unique_ptr<InstructionCache> icache, std::unique_ptr<DataCache> dcache)
    : cycles{0}
    , memory{std::move(memory)}
    , icache{std::move(icache)}
    , dcache{std::move(dcache)}
    , icachereq{0}
    , icachemiss{0}
    , dcachereq{0}
    , dcachemiss{0}
{
}

void Cpu::log(StringView message)
{
    cpusim::log("%05d - %s", cycles, message);
}

size_t Cpu::get_cycles() const
{
    return cycles;
}

void Cpu::consume(size_t cycles)
{
    this->cycles += cycles;
}

uint32_t Cpu::load(uint32_t address)
{
    if (address < registers.size())
    {
        return registers.load(this, address);
    }
    else
    {
        dcachereq += 1;
        auto request = dcache->load(this, memory, address);
        if (request.second)
        {
            dcachemiss += 1;
        }
        return request.first;
    }
}

void Cpu::store(uint32_t address, uint32_t value)
{
    if (address < registers.size())
    {
        registers.store(this, address, value);
    }
    else
    {
        memory.store(this, address, value);
    }
}

const Memory& Cpu::dump() const
{
    return memory;
}

void Cpu::execute(const Program& program)
{
    std::vector<uint32_t> memory;
    Offsets offsets;
    auto entry = program.load(memory, registers.size(), offsets);
    this->memory.store(this, registers.size(), memory);
    run(entry);
}

void Cpu::run(uint32_t entry)
{
    cycles = 0;
    const auto IP = RegisterSet::get_ref("IP");
    registers.store(this, IP, entry);

    cpusim::log("\nBEGIN PROGRAM\n");
    while (true)
    {
        auto ip = registers.load(this, IP);
        registers.store(this, IP, ip + 4);
        icachereq += 1;
        auto request = icache->load(this, memory, ip);
        if (request.second)
        {
            icachemiss += 1;
        }
        auto raw = request.first;
        instructions.execute(this, raw[0], InstructionArguments{raw[1], raw[2], raw[3]});
        if (raw[0] == InstructionSet::get_ref("HALT"))
        {
            cpusim::log("\nEND PROGRAM\n");
            return;
        }
    }
}

std::ostream& operator<<(std::ostream& stream, const Cpu& cpu)
{
    stream << "=PERFORMANCE METRICS============" << std::endl;
    stream << "Instructions executed: " << cpu.icachereq << std::endl;
    stream << "Cycles consumed: " << cpu.cycles << std::endl;
    stream << "Instruction cache requests: " << cpu.icachereq << std::endl;
    auto imissratio = (cpu.icachemiss / static_cast<double>(cpu.icachereq)) * 100.0;
    stream << "Instruction cache misses: " << cpu.icachemiss << " (" << imissratio << "%)" << std::endl;
    stream << "Data cache requests: " << cpu.dcachereq << std::endl;
    auto dmissratio = (cpu.dcachemiss / static_cast<double>(cpu.dcachereq)) * 100.0;
    stream << "Data cache misses: " << cpu.dcachemiss << " (" << dmissratio << "%)" << std::endl;
    stream << "=REGISTERS======================" << std::endl;
    stream << cpu.registers;
    return stream;
}

}
