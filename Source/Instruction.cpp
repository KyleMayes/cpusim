#include <CpuSim/Instruction.hpp>

#include <CpuSim/Cpu.hpp>
#include <CpuSim/Format.hpp>

namespace cpusim
{

Instruction::Instruction(std::string name, size_t cycles, InstructionFunction function)
    : name{std::move(name)}, cycles{cycles}, function{std::move(function)}
{
}

void Instruction::execute(Cpu* cpu, InstructionArguments arguments) const
{
    cpu->log(format("Instruction::execute(): %s [%d, %d, %d]", name, arguments.a, arguments.b, arguments.c));
    cpu->consume(cycles);
    function(cpu, arguments);
}

InstructionRef InstructionSet::get_ref(StringView name)
{
    InstructionRef counter = 0;
#define CPUSIM_INSTRUCTION(NAME, CYCLES, FUNCTION) if (name == StringView{#NAME}) { return counter; } counter += 1;
    #include <CpuSim/Instruction.inl>
#undef CPUSIM_INSTRUCTION
    log("ERROR: no such instruction: '%s'", name);
    std::exit(1);
}

InstructionSet::InstructionSet()
{
#define CPUSIM_INSTRUCTION(NAME, CYCLES, FUNCTION) instructions.emplace_back(#NAME, CYCLES, FUNCTION);
    #include <CpuSim/Instruction.inl>
#undef CPUSIM_INSTRUCTION
}

void InstructionSet::execute(Cpu* cpu, InstructionRef ref, InstructionArguments arguments) const
{
    assert(ref < instructions.size());
    instructions[ref].execute(cpu, arguments);
}

}
